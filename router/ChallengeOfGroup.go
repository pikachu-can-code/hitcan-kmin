package router

import (
	"fmt"
	"hitcan-kmin/handlers"
	"hitcan-kmin/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func ChallengeOfGroup(c *gin.Context) {
	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = handlers.ParseTokenString(tokenString)
		if err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	} else {
		account.ID = 0
	}

	groupID, err := strconv.Atoi(c.Query("group"))
	if err != nil {
		panic(err)
	}

	group := models.Group{}
	group.ID = uint(groupID)

	if err := group.GetGroup(); err != nil {
		panic(err)
	}

	c.HTML(http.StatusOK, "challenge_of_group.html", gin.H{
		"User":  account,
		"Group": group,
	})
}
