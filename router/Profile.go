package router

import (
	"fmt"
	"hitcan-kmin/handlers"
	"hitcan-kmin/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

func UserProfile(c *gin.Context) {
	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = handlers.ParseTokenString(tokenString)
		if err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	} else {
		PageNotFound(c)
		return
	}

	c.HTML(http.StatusOK, "profile.html", account)
}
