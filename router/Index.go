package router

import (
	"fmt"
	"hitcan-kmin/handlers"
	"hitcan-kmin/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type data struct {
	User       models.Account
	Topics     models.ListTopics
	PageIndex  int
	NumOfPages int
	Left       int
	Right      int
	Pages      []int
}

func Index(c *gin.Context) {

	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = handlers.ParseTokenString(tokenString)
		if err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	}

	account := models.Account{}
	topics := models.ListTopics{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	} else {
		account.ID = 0
	}

	total := topics.CountTopic()

	limit := 9
	pageIndex, begin := Pagination(c, limit)
	numOfPages := total / limit

	if err := topics.GetTopicLimit(begin, limit); err != nil {
		fmt.Println(err)
		PageNotFound(c)
		return
	}

	var arrPage []int
	var left, right int

	arrPage = GetPageToShow(numOfPages, pageIndex)

	left = pageIndex - 1
	right = pageIndex + 1

	datares := data{
		User:       account,
		Topics:     topics,
		PageIndex:  pageIndex,
		NumOfPages: numOfPages,
		Left:       left,
		Right:      right,
		Pages:      arrPage,
	}

	c.HTML(http.StatusOK, "index.html", datares)
}
