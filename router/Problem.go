package router

import (
	"fmt"
	"hitcan-kmin/handlers"
	"hitcan-kmin/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type dataResponseProblem struct {
	User      models.Account
	Challenge models.Challenge
	NameTopic string `json:"name_topic,omitempty"`
	IDTopic   uint   `json:"id_topic,omitempty"`
}

func ProblemPage(c *gin.Context) {
	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = handlers.ParseTokenString(tokenString)
		if err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	} else {
		account.ID = 0
	}

	challengeID, err := strconv.Atoi(c.Query("id"))
	if err != nil {
		fmt.Println(err)
		PageNotFound(c)
		return
	}

	nameTopic := c.Query("name_topic")
	idTopic, err := strconv.Atoi(c.Query("id_topic"))
	if err != nil {
		challenge := models.Challenge{}
		challenge.ID = uint(challengeID)

		if account.Premium == 0 || account.ID == 0 {
			if err := challenge.GetChallengeAccountNormal(); err != nil {
				fmt.Println(err)
				PageNotFound(c)
				return
			}
		} else {
			if err := challenge.GetChallengeAccountPremium(); err != nil {
				fmt.Println(err)
				PageNotFound(c)
				return
			}
		}

		dataRes := dataResponseProblem{
			User:      account,
			Challenge: challenge,
			NameTopic: nameTopic,
			IDTopic:   uint(idTopic),
		}

		c.HTML(http.StatusOK, "problem.html", dataRes)
		return
	}

	challenge := models.Challenge{}
	challenge.ID = uint(challengeID)

	if account.Premium == 0 || account.ID == 0 {
		if err := challenge.GetChallengeAccountNormal(); err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	} else {
		if err := challenge.GetChallengeAccountPremium(); err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	}

	dataRes := dataResponseProblem{
		User:      account,
		Challenge: challenge,
		NameTopic: nameTopic,
		IDTopic:   uint(idTopic),
	}

	c.HTML(http.StatusOK, "problem.html", dataRes)
}
