var load = `<span class="spinner-border text-warning spinner-border-sm" role="status" aria-hidden="true"></span>
Loading...`;
var pageIndexUser = 1;
var pageIndexFeedback = 1;
var maxTimeLoadFeedback;
var maxTimeLoad;
var groups;
var countFirstTime = 0;

$(document).ready(function () {
    $("#add-new-group").on("show.bs.modal", function (e) {
        var button = $(e.relatedTarget);
        var recipient = button.data("whatever");
        var todo = button.data("id");
        var dataName = button.data("name");
        var modal = $(this);
        const header = getCookie("Authorization");
        if (dataName != undefined) {
            var idGroup = button.data("id-group");
            const requestUsers = axios.get("http://hit.kmin.edu.vn/account/api/get_all_account", {
                headers: { Authorization: header },
            });
            const requestUsersOfGroup = axios.get("http://hit.kmin.edu.vn/group/api/get_account_of_group/" + idGroup, {
                headers: { Authorization: header },
            });
            const requestChallenges = axios.get("http://hit.kmin.edu.vn/challenge/api/get_all_challenges", {
                headers: { Authorization: header },
            });
            axios
                .all([requestUsers, requestChallenges, requestUsersOfGroup])
                .then(
                    axios.spread((resUsers, resChallenges, resUsersOfGroup) => {
                        $("#member").empty().trigger("change");
                        $("#challenges-group").empty().trigger("change");
                        var selectedUser = resUsersOfGroup.data.data.group.accounts_of_group;
                        var allUsers = resUsers.data.data.list_accounts;
                        if (selectedUser == undefined) {
                            selectedUser = [];
                        }
                        var dataSelect2 = allUsers.map((user) => {
                            const selectedID = selectedUser.find((userFind) => userFind.ID == user.ID);
                            return {
                                id: user.ID,
                                text: user.email,
                                selected: selectedID,
                            };
                        });
                        $("#member").select2({
                            data: dataSelect2,
                        });

                        var selectedChallenges = resUsersOfGroup.data.data.group.exercises_in_group;
                        var allChallenge = resChallenges.data.data.challenges;
                        if (selectedChallenges == undefined) {
                            selectedChallenges = [];
                        }
                        var dataSelect22 = allChallenge.map((challenge) => {
                            const selectedID = selectedChallenges.find(
                                (challengeFind) => challengeFind.ID == challenge.ID
                            );
                            return {
                                id: challenge.ID,
                                text: challenge.name_challenge,
                                selected: selectedID,
                            };
                        });
                        $("#challenges-group").select2({
                            data: dataSelect22,
                        });
                    })
                )
                .catch((err) => {
                    $("#noti-content").html(
                        err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
                    );
                    $("#notification").toast("show");
                });
            modal.find("#group-name").val(dataName);
            modal.find("#create-change-group").attr("old-name", dataName);
            modal.find("#create-change-group").attr("data-change", dataName);
            modal.find("#create-change-group").text("Thay đổi");
            modal.find("#create-change-group").attr("id-group", idGroup);
        } else {
            modal.find("#create-change-group").text("Tạo nhóm");
            modal.find("#group-name").val("");
            axios
                .get("http://hit.kmin.edu.vn/account/api/get_all_account", { headers: { Authorization: header } })
                .then((res) => {
                    $("#member").empty().trigger("change");
                    var data = res.data.data.list_accounts;
                    var dataRender = data.map((element) => {
                        return {
                            id: element.ID,
                            text: element.email,
                        };
                    });
                    $("#member").select2({
                        data: dataRender,
                    });
                })
                .catch((err) => {
                    $("#noti-content").html(
                        err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
                    );
                    $("#notification").toast("show");
                });
            axios
                .get("http://hit.kmin.edu.vn/challenge/api/get_all_challenges", { headers: { Authorization: header } })
                .then((res) => {
                    $("#challenges-group").empty().trigger("change");
                    var data = res.data.data.challenges;
                    var dataRender = data.map((element) => {
                        return {
                            id: element.ID,
                            text: element.name_challenge,
                        };
                    });
                    $("#challenges-group").select2({
                        data: dataRender,
                    });
                });
        }

        modal.find(".modal-title").text(recipient);
        modal.find("#create-change-group").attr("data-todo", todo);
    });

    $("#create-change-group").on("click", function () {
        var todo = $(this).attr("data-todo");
        const header = getCookie("Authorization");
        if (todo == "create-group") {
            var name = $("#group-name").val();
            var users = $("#member").val();
            var challenges = $("#challenges-group").val();
            var idUsers = users.map((data) => {
                return {
                    ID: parseInt(data),
                };
            });
            var idChallenges = challenges.map((data) => {
                return {
                    ID: parseInt(data),
                };
            });
            name = name.trim();
            if (name != "") {
                $(this).html("").append(load);
                var dataBody = {
                    group_name: name,
                    accounts_of_group: idUsers,
                    exercises_in_group: idChallenges,
                };
                axios({
                    method: "post",
                    headers: { Authorization: header },
                    url: "http://hit.kmin.edu.vn/group/api/create_group",
                    data: dataBody,
                })
                    .then((res) => {
                        $(this).html("").text("Tạo Group");

                        var groupResponse = res.data.data.group;

                        var DOM = `
                        <div class="au-task__item au-task__item--danger remove-group-${groupResponse.ID}">
                            <div class="au-task__item-inner d-flex justify-content-between align-items-center">
                                <div>
                                    <h5 class="task">
                                        <a href="#!" class="data-name-group" data-toggle="modal" data-target="#add-new-group" data-whatever="Chỉnh Sửa Group" data-id="edit-group" data-id-group="${groupResponse.ID}" data-name="${groupResponse.group_name}">${groupResponse.group_name}</a>
                                    </h5>
                                </div>
                                <button class="btn btn-light delete-group" onclick="deleteGroup(${groupResponse.ID})">
                                    <i class="far fa-trash-alt text-danger"></i>
                                </button>
                            </div>
                        </div>
                        `;
                        $("#groups-dom").append(DOM);
                        $("#render-user").html("");
                        loadUserData();
                    })
                    .catch((err) => {
                        $(this).html("").text("Tạo Group");
                        $("#noti-content").html(
                            err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
                        );
                        $("#notification").toast("show");
                    });
            }
        } else if (todo == "edit-group") {
            $(this).html("").append(load);
            var name = $("#group-name").val();
            var idGroup = $(this).attr("id-group");
            var users = $("#member").val();
            var challenges = $("#challenges-group").val();
            var idUsers = users.map((data) => {
                return {
                    ID: parseInt(data),
                };
            });
            var idChallenges = challenges.map((data) => {
                return {
                    ID: parseInt(data),
                };
            });
            name = name.trim();
            var dataBody = {
                ID: parseInt(idGroup),
                group_name: name,
                accounts_of_group: idUsers,
                exercises_in_group: idChallenges,
            };
            axios({
                method: "put",
                headers: { Authorization: header },
                url: "http://hit.kmin.edu.vn/group/api/update_group",
                data: dataBody,
            })
                .then((res) => {
                    $(this).attr("old-name", res.data.data.group.group_name);
                    $("#render-user").html("");
                    loadUserData();
                    $(this).html("").text("Thay đổi");
                })
                .catch((err) => {
                    $(this).html("").text("Thay đổi");
                    $("#noti-content").html(
                        err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
                    );
                    $("#notification").toast("show");
                });
        }
    });

    $('.groups-of-accounts:select[name="groups"]').on("select2:select", function (e) {
        var groupSelected = $(e.currentTarget).find("option:selected").val();
    });

    try {
        $('[data-toggle="tooltip"]').tooltip();
    } catch (error) {
        console.log(error);
    }

    // Chatbox
    // var inbox_wrap = $(".js-inbox");

    $("#back").on("click", function () {
        $(this).parent().parent().parent().parent()[0].classList.remove("show-chat-box");
    });
});

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element);
    node.classList.add("animated", animationName);

    function handleAnimationEnd() {
        node.classList.remove("animated", animationName);
        node.removeEventListener("animationend", handleAnimationEnd);

        if (typeof callback === "function") callback();
    }

    node.addEventListener("animationend", handleAnimationEnd);
}

function deleteAccount(idUser) {
    if (confirm("Bạn muốn xóa tài khoảng này?")) {
        const header = getCookie("Authorization");
        axios({
            headers: {
                Authorization: header,
            },
            method: "delete",
            url: "http://hit.kmin.edu.vn/account/api/delete_account?id_user=" + idUser,
        })
            .then((res) => {
                animateCSS(".user-remove-" + idUser, "bounceOut", function () {
                    $(".user-remove-" + idUser).addClass("d-none");
                    var numberUser = $("#number-accounts").text();
                    var numberUserInt = parseInt(numberUser);
                    numberUserInt--;
                    $("#number-accounts").text(numberUserInt);
                });
            })
            .catch((err) => {
                $("#noti-content").html(
                    err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
                );
                $("#notification").toast("show");
            });
    }
}

function deleteGroup(idGroup) {
    if (confirm("Bạn muốn xóa group này?")) {
        const header = getCookie("Authorization");
        axios({
            headers: {
                Authorization: header,
            },
            method: "delete",
            url: "http://hit.kmin.edu.vn/group/api/delete_group?id_group=" + idGroup,
        })
            .then((res) => {
                $("#render-user").html("");
                loadUserData();
                animateCSS(".remove-group-" + idGroup, "bounceOutRight", function () {
                    $(".remove-group-" + idGroup).addClass("d-none");
                });
            })
            .catch((err) => {
                $("#noti-content").html(
                    err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
                );
                $("#notification").toast("show");
            });
    }
}

loadUserData();

function loadUserData() {
    const header = getCookie("Authorization");
    const requestUsers = axios.get("http://hit.kmin.edu.vn/account/api/get_account_limit/" + pageIndexUser, {
        headers: { Authorization: header },
    });
    const requestGroups = axios.get("http://hit.kmin.edu.vn/group/api/get_all_groups", {
        headers: { Authorization: header },
    });

    axios
        .all([requestUsers, requestGroups])
        .then(
            axios.spread((...response) => {
                pageIndexUser++;
                // if (pageIndexUser <= resUsers.max_time_load) {
                //   pageIndexUser++;
                // }
                const users = response[0].data.data.accounts;
                const allGroups = response[1].data.data.groups;

                maxTimeLoad = response[0].data.data.max_time_load;

                const userMap = users.map((user) => {
                    const name = user.name;
                    const ID = user.ID;
                    const premium = user.premium;
                    const email = user.email;
                    let groupsUser = user.groups_of_account;
                    if (groupsUser == undefined) {
                        groupsUser = [];
                    }
                    const allGroupsMap = allGroups.map((group) => {
                        return `<option value="${group.ID}" ${
                            groupsUser.find((element) => element.ID == group.ID) ? "selected" : ""
                        }>${group.group_name}</option>`;
                    });
                    return `
                        <tr class="user-remove-${ID}">
                            <td>
                                <label class="au-checkbox">
                                    <input type="checkbox" ${
                                        premium == 1 ? "checked" : ""
                                    } id="${ID}" onclick="checkboxPremium(${ID})" />
                                    <span class="au-checkmark premium"></span>
                                </label>
                            </td>
                            <td>
                                <div class="table-data__info">
                                    <h6>${name}</h6>
                                    <span>
                                        <a href="mailto:${email}?Subject=Hello">${email}</a>
                                    </span>
                                </div>
                            </td>
                            <td>
                                <span>${user.phone ? user.phone : "Không có"}</span>
                            </td>
                            <td>
                                <div class="rs-select2--trans rs-select2--sm w-100">
                                    <select class="js-select2 groups-of-accounts" name="groups" onselect="updateUserGroup(${ID})" multiple>
                                        ${allGroupsMap}
                                    </select>
                                    <div class="dropDownSelect2"></div>
                                </div>
                            </td>
                            <td>
                                <button onclick="deleteAccount(${ID})" class="more" data-toggle="tooltip" data-placement="top" title="Xóa tài khoản này">
                                    <i class="far fa-trash-alt text-danger"></i>
                                </button>
                            </td>
                        </tr>`;
                });

                $("#render-user").append(userMap);
                $(".groups-of-accounts").each(function () {
                    $(this).select2({
                        minimumResultsForSearch: 100,
                        dropdownParent: $(this).next(".dropDownSelect2"),
                        selectOnClose: true,
                        closeOnSelect: false,
                    });
                });
                $("#user-loadmore").html("").text("load more");

                if (pageIndexUser > maxTimeLoad) {
                    $("#user-loadmore").addClass("d-none");
                }

                $("#user-loadmore").on("click", function () {
                    if (pageIndexUser <= maxTimeLoad) {
                        $(this).html("").text("loading...");
                        loadUserData();
                    }
                });
            })
        )
        .catch((err) => {
            $("#noti-content").html(err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu");
            $("#notification").toast("show");
        });
}
getFeedBack();
function getFeedBack() {
    const header = getCookie("Authorization");
    axios({
        method: "get",
        headers: {
            Authorization: header,
        },
        url: "http://hit.kmin.edu.vn/feedback/api/get_all_feedback/" + pageIndexFeedback,
    }).then((res) => {
        pageIndexFeedback++;
        var data = res.data.data.list.list_feedback;
        maxTimeLoadFeedback = res.data.data.max_page;
        var render = data.map((element) => {
            return `
            <div class="au-message__item ${element.feedback.seen == false ? "unread" : ""}" id-feedback="${
                element.feedback.ID
            }" id-account="${element.account.ID}" id-problem="${element.problem.ID}" >
                <div class="au-message__item-inner">
                    <div class="au-message__item-text w-75">
                        <div class="avatar-wrap">
                            <div class="avatar">
                                <img src="${element.account.url_avatar}" alt="${element.account.name}" />
                            </div>
                        </div>
                        <div class="text">
                            <h5 class="name">${element.account.name}</h5>
                            <p>${
                                element.feedback.understanding == 0
                                    ? "Không hiểu"
                                    : element.feedback.understanding == 1
                                    ? "Tạm hiểu"
                                    : "Rất hiểu"
                            }</p>
                            <p class="downline-dev">
                                ${element.feedback.comment ? element.feedback.comment : ""}
                            </p>
                        </div>
                    </div>
                    <div class="au-message__item-time w-25">
                        <span>${element.problem.name_problem}</span>
                    </div>
                </div>
            </div>`;
        });

        $("#noti-feedback").append(render);
        $(".au-message__item").on("click", function () {
            var requestData = {
                id_account: parseInt($(this).attr("id-account")),
                id_problem: parseInt($(this).attr("id-problem")),
                id_feedback: parseInt($(this).attr("id-feedback")),
            };
            axios({
                method: "post",
                headers: {
                    Authorization: header,
                },
                url: "http://hit.kmin.edu.vn/feedback/api/get_detail_feedback",
                data: requestData,
            })
                .then((res) => {
                    data = res.data.data;
                    $("#avt-account").attr("src", data.account.url_avatar);
                    $("#name-account").html(data.account.name);
                    $("#mail-account").html(data.account.mail);
                    switch (data.feedback.understanding) {
                        case 0:
                            $("#understanding").html("Chưa hiểu");
                            break;
                        case 1:
                            $("#understanding").html("Hiểu sơ sơ");
                            break;
                        case 3:
                            $("#understanding").html("Rất hiểu");
                            break;
                    }
                    $("#comment").html("");
                    $("#comment").html(data.feedback.comment);
                    $("#problem").html(data.problem.name_problem);
                    $("#challenges").empty();
                    for (val of data.problem.challenges_of_problem) {
                        $("#challenges").append(val.name_challenge + "<br/>");
                    }

                    $(this).removeClass("unread");

                    getNotiNotSeen();

                    $(this).parent().parent().parent().toggleClass("show-chat-box");
                })
                .catch((err) => {
                    $("#noti-content").html(
                        err.response.data ? err.response.data.data.error.message : "Không xử lý được yêu cầu"
                    );
                    $("#notification").toast("show");
                });
        });

        $("#loadmore-feedback").html("").text("load more");

        if (pageIndexFeedback > maxTimeLoadFeedback) {
            $("#loadmore-feedback").addClass("d-none");
        }
        $("#loadmore-feedback").on("click", function () {
            if (pageIndexFeedback <= maxTimeLoadFeedback) {
                $(this).html("").text("loading...");
                getFeedBack();
            }
        });
    });
}
getNotiNotSeen();
function getNotiNotSeen() {
    const header = getCookie("Authorization");
    axios({
        method: "get",
        headers: {
            Authorization: header,
        },
        url: "http://hit.kmin.edu.vn/feedback/api/get_number_notseen",
    }).then((res) => {
        var data = res.data.data.number_feedback_notseen;
        $("#not-seen").html(data);
        $(".noti-number").html(data);
    });
}

function checkboxPremium(idUser) {
    if ($(`#${idUser}`).prop("checked")) {
        const header = getCookie("Authorization");
        axios({
            method: "put",
            headers: {
                Authorization: header,
            },
            url: "http://hit.kmin.edu.vn/account/api/update_premium",
            data: {
                ID: idUser,
                premium: 1,
            },
        }).catch((err) => {
            $("#noti-content").html(err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu");
            $("#notification").toast("show");
        });
    } else {
        const header = getCookie("Authorization");
        axios({
            method: "put",
            headers: {
                Authorization: header,
            },
            url: "http://hit.kmin.edu.vn/account/api/update_premium",
            data: {
                ID: idUser,
                premium: 0,
            },
        }).catch((err) => {
            $("#noti-content").html(err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu");
            $("#notification").toast("show");
        });
    }
}

function updateUserGroup(id) {
    alert(id);
}
