let premium = $("#dropdown1").attr("premium");
let endLoad, idChallenge, idTopic, nameChallenge, nameTopic;
let loadOneTime = 0;
let countTimeLoad = 1;

if (premium === undefined) {
    premium = 0;
}

// TODO hàm render ra những bài tập dễ
const renderExercisesEasy = (exercisesObj) => {
    let x;
    for (x in exercisesObj) {
        if (exercisesObj[x].level == 1) {
            if ((premium == 0 && exercisesObj[x].premium == 0) || premium == 1) {
                $("#exercises-problem").append(`
      <div class="col l6 m12 s12 easy">
        <div class="card-problem">
          <div class="face face1">
            <div class="card-content">
              <span class="bold">Độ khó: </span
              ><span class="cyan-text darken-4">Dễ</span>
              <a href="/detail_problem?id=${exercisesObj[x].ID}&id_topic=${idTopic}&id_challenge=${idChallenge}&name_topic=${nameTopic}&name_challenge=${nameChallenge}">Solve problem</a>
            </div>
          </div>
          <div class="face face2">
            <p>${exercisesObj[x].name_problem}</p>
          </div>
        </div>
      </div>`);
            } else if (premium == 0 && exercisesObj[x].premium == 1) {
                $("#exercises-problem").append(`<div class="col l6 m12 s12 easy">
      <div class="card-problem">
        <div class="face face1">
          <div class="card-content">
            <span class="bold">Độ khó: </span
            ><span class="cyan-text darken-4">Dễ</span>
            
          </div>
        </div>
        <div class="face face2">
          <p>${exercisesObj[x].name_problem}</p>
        </div>
      </div>
      <div class="lock">
        <div class="lock-background"></div>
        <div class="lock-content">
          <i class="fas fa-lock"></i>
          <p>Premium Only</p>
          </div>
        </div>
      </div>`);
            }
        }
    }
};

// TODO hàm render ra những bài tập bình thường
const renderExercisesNormal = (exercisesObj) => {
    let x;
    for (x in exercisesObj) {
        if (exercisesObj[x].level == 2) {
            if ((premium == 0 && exercisesObj[x].premium == 0) || premium == 1) {
                $("#exercises-problem").append(`<div class="col l6 m12 s12 normal">
    <div class="card-problem">
      <div class="face face1">
        <div class="card-content">
          <span class="bold">Độ khó: </span
          ><span class="green-text darken-4">Bình thường</span>
          <a href="/detail_problem?id=${exercisesObj[x].ID}&id_topic=${idTopic}&id_challenge=${idChallenge}&name_topic=${nameTopic}&name_challenge=${nameChallenge}">Solve problem</a>
        </div>
      </div>
      <div class="face face2">
        <p>${exercisesObj[x].name_problem}</p>
      </div>
    </div>
    </div>`);
            } else if (premium == 0 && exercisesObj[x].premium == 1) {
                $("#exercises-problem").append(`<div class="col l6 m12 s12 normal">
    <div class="card-problem">
      <div class="face face1">
        <div class="card-content">
          <span class="bold">Độ khó: </span
          ><span class="green-text darken-4">Bình thường</span>
          <a href="/detail_problem?id=${exercisesObj[x].ID}&id_topic=${idTopic}&id_challenge=${idChallenge}&name_topic=${nameTopic}&name_challenge=${nameChallenge}"
            >Solve problem</a
          >
        </div>
      </div>
      <div class="face face2">
        <p>${exercisesObj[x].name_problem}</p>
      </div>
    </div>
    <div class="lock">
      <div class="lock-background"></div>
      <div class="lock-content">
        <i class="fas fa-lock"></i>
        <p>Premium Only</p>
        </div>
      </div>
    </div>`);
            }
        }
    }
};

// TODO hàm render ra những bài tập Khó
const renderExercisesHard = (exercisesObj) => {
    let x;
    for (x in exercisesObj) {
        if (exercisesObj[x].level == 3) {
            if ((premium == 0 && exercisesObj[x].premium == 0) || premium == 1) {
                $("#exercises-problem").append(`<div class="col l6 m12 s12 hard">
      <div class="card-problem">
        <div class="face face1">
          <div class="card-content">
            <span class="bold">Độ khó: </span
            ><span class="orange-text darken-4">Khó</span>
            <a href="/detail_problem?id=${exercisesObj[x].ID}&id_topic=${idTopic}&id_challenge=${idChallenge}&name_topic=${nameTopic}&name_challenge=${nameChallenge}">Solve problem</a>
          </div>
        </div>
        <div class="face face2">
          <p>${exercisesObj[x].name_problem}</p>
        </div>
      </div>
    </div>`);
            } else if (premium == 0 && exercisesObj[x].premium == 1) {
                $("#exercises-problem").append(`<div class="col l6 m12 s12 hard">
      <div class="card-problem">
        <div class="face face1">
          <div class="card-content">
            <span class="bold">Độ khó: </span
            ><span class="orange-text darken-4">Khó</span>
            
          </div>
        </div>
        <div class="face face2">
          <p>${exercisesObj[x].name_problem}</p>
        </div>
      </div>
      <div class="lock">
        <div class="lock-background"></div>
        <div class="lock-content">
          <i class="fas fa-lock"></i>
          <p>Premium Only</p>
          </div>
        </div>
      </div>`);
            }
        }
    }
};

// TODO hàm render ra những bài tập cực khó
const renderExercisesExtreme = (exercisesObj) => {
    let x;
    for (x in exercisesObj) {
        if (exercisesObj[x].level == 4) {
            if ((premium == 0 && exercisesObj[x].premium == 0) || premium == 1) {
                $("#exercises-problem").append(`<div class="col l6 m12 s12 extreme">
      <div class="card-problem">
        <div class="face face1">
          <div class="card-content">
            <span class="bold">Độ khó: </span
            ><span class="red-text darken-4">Cực khó</span>
            <a href="/detail_problem?id=${exercisesObj[x].ID}&id_topic=${idTopic}&id_challenge=${idChallenge}&name_topic=${nameTopic}&name_challenge=${nameChallenge}">Solve problem</a>
          </div>
        </div>
        <div class="face face2">
          <p>${exercisesObj[x].name_problem}</p>
        </div>
      </div>
    </div>`);
            } else if (premium == 0 && exercisesObj[x].premium == 1) {
                $("#exercises-problem").append(`<div class="col l6 m12 s12 extreme">
      <div class="card-problem">
        <div class="face face1">
          <div class="card-content">
            <span class="bold">Độ khó: </span
            ><span class="orange-text darken-4">Khó</span>
            
          </div>
        </div>
        <div class="face face2">
          <p>${exercisesObj[x].name_problem}</p>
        </div>
      </div>
      <div class="lock">
        <div class="lock-background"></div>
        <div class="lock-content">
          <i class="fas fa-lock"></i>
          <p>Premium Only</p>
          </div>
        </div>
      </div>`);
            }
        }
    }
};

// TODO hàm render ra full bài tập
const renderExercisesAll = (exercisesObj) => {
    let x;
    for (x in exercisesObj) {
        if (exercisesObj[x].level == 1) {
            if ((premium == 0 && exercisesObj[x].premium == 0) || premium == 1) {
                $("#exercises-problem").append(`
      <div class="col l6 m12 s12 easy">
        <div class="card-problem">
          <div class="face face1">
            <div class="card-content">
              <span class="bold">Độ khó: </span
              ><span class="cyan-text darken-4">Dễ</span>
              <a href="/detail_problem?id=${exercisesObj[x].ID}&id_topic=${idTopic}&id_challenge=${idChallenge}&name_topic=${nameTopic}&name_challenge=${nameChallenge}">Solve problem</a>
            </div>
          </div>
          <div class="face face2">
            <p>${exercisesObj[x].name_problem}</p>
          </div>
        </div>
      </div>`);
            } else if (premium == 0 && exercisesObj[x].premium == 1) {
                $("#exercises-problem").append(`<div class="col l6 m12 s12 easy">
      <div class="card-problem">
        <div class="face face1">
          <div class="card-content">
            <span class="bold">Độ khó: </span
            ><span class="cyan-text darken-4">Dễ</span>
            
          </div>
        </div>
        <div class="face face2">
          <p>${exercisesObj[x].name_problem}</p>
        </div>
      </div>
      <div class="lock">
        <div class="lock-background"></div>
        <div class="lock-content">
          <i class="fas fa-lock"></i>
          <p>Premium Only</p>
          </div>
        </div>
      </div>`);
            }
        } else if (exercisesObj[x].level == 2) {
            if ((premium == 0 && exercisesObj[x].premium == 0) || premium == 1) {
                $("#exercises-problem").append(`<div class="col l6 m12 s12 normal">
      <div class="card-problem">
        <div class="face face1">
          <div class="card-content">
            <span class="bold">Độ khó: </span
            ><span class="green-text darken-4">Bình thường</span>
            <a href="/detail_problem?id=${exercisesObj[x].ID}&id_topic=${idTopic}&id_challenge=${idChallenge}&name_topic=${nameTopic}&name_challenge=${nameChallenge}">Solve problem</a>
          </div>
        </div>
        <div class="face face2">
          <p>${exercisesObj[x].name_problem}</p>
        </div>
      </div>
      </div>`);
            } else if (premium == 0 && exercisesObj[x].premium == 1) {
                $("#exercises-problem").append(`<div class="col l6 m12 s12 normal">
      <div class="card-problem">
        <div class="face face1">
          <div class="card-content">
            <span class="bold">Độ khó: </span
            ><span class="green-text darken-4">Bình thường</span>
            
          </div>
        </div>
        <div class="face face2">
          <p>${exercisesObj[x].name_problem}</p>
        </div>
      </div>
      <div class="lock">
        <div class="lock-background"></div>
        <div class="lock-content">
          <i class="fas fa-lock"></i>
          <p>Premium Only</p>
          </div>
        </div>
      </div>`);
            }
        } else if (exercisesObj[x].level == 3) {
            if ((premium == 0 && exercisesObj[x].premium == 0) || premium == 1) {
                $("#exercises-problem").append(`<div class="col l6 m12 s12 hard">
      <div class="card-problem">
        <div class="face face1">
          <div class="card-content">
            <span class="bold">Độ khó: </span
            ><span class="orange-text darken-4">Khó</span>
            <a href="/detail_problem?id=${exercisesObj[x].ID}&id_topic=${idTopic}&id_challenge=${idChallenge}&name_topic=${nameTopic}&name_challenge=${nameChallenge}">Solve problem</a>
          </div>
        </div>
        <div class="face face2">
          <p>${exercisesObj[x].name_problem}</p>
        </div>
      </div>
    </div>`);
            } else if (premium == 0 && exercisesObj[x].premium == 1) {
                $("#exercises-problem").append(`<div class="col l6 m12 s12 hard">
      <div class="card-problem">
        <div class="face face1">
          <div class="card-content">
            <span class="bold">Độ khó: </span
            ><span class="orange-text darken-4">Khó</span>
            
          </div>
        </div>
        <div class="face face2">
          <p>${exercisesObj[x].name_problem}</p>
        </div>
      </div>
      <div class="lock">
        <div class="lock-background"></div>
        <div class="lock-content">
          <i class="fas fa-lock"></i>
          <p>Premium Only</p>
          </div>
        </div>
      </div>`);
            }
        } else if (exercisesObj[x].level == 4) {
            if ((premium == 0 && exercisesObj[x].premium == 0) || premium == 1) {
                $("#exercises-problem").append(`<div class="col l6 m12 s12 extreme">
      <div class="card-problem">Frontend
        <div class="face face1">
          <div class="card-content">
            <span class="bold">Độ khó: </span
            ><span class="red-text darken-4">Cực khó</span>
            <a href="/detail_problem?id=${exercisesObj[x].ID}&id_topic=${idTopic}&id_challenge=${idChallenge}&name_topic=${nameTopic}&name_challenge=${nameChallenge}">Solve problem</a>
          </div>
        </div>
        <div class="face face2">
          <p>${exercisesObj[x].name_problem}</p>
        </div>
      </div>
    </div>`);
            } else if (premium == 0 && exercisesObj[x].premium == 1) {
                $("#exercises-problem").append(`<div class="col l6 m12 s12 extreme">
      <div class="card-problem">
        <div class="face face1">
          <div class="card-content">
            <span class="bold">Độ khó: </span
            ><span class="orange-text darken-4">Khó</span>
            
          </div>
        </div>
        <div class="face face2">
          <p>${exercisesObj[x].name_problem}</p>
        </div>
      </div>
      <div class="lock">
        <div class="lock-background"></div>
        <div class="lock-content">
          <i class="fas fa-lock"></i>
          <p>Premium Only</p>
          </div>
        </div>
      </div>`);
            }
        }
    }
};

async function getData(timeLoad) {
    let data;
    let idChallenge = $("#index-now").attr("id-challenge");
    await axios({
        method: "get",
        url: `http://hit.kmin.edu.vn/challenge/api/get_challenge/${idChallenge}/${timeLoad}`,
    })
        .then((res) => {
            data = res.data.data.problems_of_challenge;
            endLoad = res.data.max_time_load;
        })
        .catch((err) => {
            console.log(err);
        });
    return data;
}

$(document).ready(() => {
    $(".modal").modal();

    let count = 0;
    let challenges = [];
    getData(countTimeLoad).then((res) => {
        challenges.push(res);
        countTimeLoad++;
        idTopic = $("#index-before").attr("id-topic");
        idChallenge = $("#index-now").attr("id-challenge");
        nameTopic = $("#index-before").text();
        nameChallenge = $("#index-now").text();

        for (value of challenges) {
            renderExercisesAll(value);
        }
        Scroll(challenges);
        $("#easy").on("change", () => {
            if ($("#easy").is(":checked")) {
                $("#easy-mobile").attr("checked", true);
                if (count == 0) {
                    $("#exercises-problem").empty();
                }
                for (value of challenges) {
                    renderExercisesEasy(value);
                }
                count++;
            } else {
                $("#easy-mobile").attr("checked", false);
                $(".easy").remove();
                count--;
            }
            if (count == 0) {
                for (value of challenges) {
                    renderExercisesAll(value);
                }
            }
        });

        $("#easy-mobile").on("change", () => {
            if ($("#easy-mobile").is(":checked")) {
                $("#easy").attr("checked", true);
                if (count == 0) {
                    $("#exercises-problem").empty();
                }
                for (value of challenges) {
                    renderExercisesEasy(value);
                }
                count++;
            } else {
                $("#easy").attr("checked", false);
                $(".easy").remove();
                count--;
            }
            if (count == 0) {
                for (value of challenges) {
                    renderExercisesAll(value);
                }
            }
        });

        $("#normal").on("change", () => {
            if ($("#normal").is(":checked")) {
                $("#normal-mobile").attr("checked", true);
                if (count == 0) {
                    $("#exercises-problem").empty();
                }
                for (value of challenges) {
                    renderExercisesNormal(value);
                }
                count++;
            } else {
                $("#normal-mobile").attr("checked", false);
                $(".normal").remove();
                count--;
            }
            if (count == 0) {
                for (value of challenges) {
                    renderExercisesAll(value);
                }
            }
        });

        $("#normal-mobile").on("change", () => {
            if ($("#normal-mobile").is(":checked")) {
                $("#normal").attr("checked", true);
                if (count == 0) {
                    $("#exercises-problem").empty();
                }
                for (value of challenges) {
                    renderExercisesNormal(value);
                }
                count++;
            } else {
                $("#normal").attr("checked", false);
                $(".normal").remove();
                count--;
            }
            if (count == 0) {
                for (value of challenges) {
                    renderExercisesAll(value);
                }
            }
        });

        $("#hard").on("change", () => {
            if ($("#hard").is(":checked")) {
                $("#hard-mobile").attr("checked", true);
                if (count == 0) {
                    $("#exercises-problem").empty();
                }
                for (value of challenges) {
                    renderExercisesHard(value);
                }
                count++;
            } else {
                $("#hard-mobile").attr("checked", false);
                $(".hard").remove();
                count--;
            }
            if (count == 0) {
                for (value of challenges) {
                    renderExercisesAll(value);
                }
            }
        });

        $("#hard-mobile").on("change", () => {
            if ($("#hard-mobile").is(":checked")) {
                $("#hard").attr("checked", true);
                if (count == 0) {
                    $("#exercises-problem").empty();
                }
                for (value of challenges) {
                    renderExercisesHard(value);
                }
                count++;
            } else {
                $("#hard").attr("checked", false);
                $(".hard").remove();
                count--;
            }
            if (count == 0) {
                for (value of challenges) {
                    renderExercisesAll(value);
                }
            }
        });

        $("#extreme").on("change", () => {
            if ($("#extreme").is(":checked")) {
                $("#extreme-mobile").attr("checked", true);
                if (count == 0) {
                    $("#exercises-problem").empty();
                }
                for (value of challenges) {
                    renderExercisesExtreme(value);
                }
                count++;
            } else {
                $("#extreme-mobile").attr("checked", false);
                $(".extreme").remove();
                count--;
            }
            if (count == 0) {
                for (value of challenges) {
                    renderExercisesAll(value);
                }
            }
        });

        $("#extreme-mobile").on("change", () => {
            if ($("#extreme-mobile").is(":checked")) {
                $("#extreme").attr("checked", true);
                if (count == 0) {
                    $("#exercises-problem").empty();
                }
                for (value of challenges) {
                    renderExercisesExtreme(value);
                }
                count++;
            } else {
                $("#extreme").attr("checked", false);
                $(".extreme").remove();
                count--;
            }
            if (count == 0) {
                for (value of challenges) {
                    renderExercisesAll(value);
                }
            }
        });
    });
});

function Scroll(challenges) {
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 100 && countTimeLoad <= endLoad) {
            if (loadOneTime == 0) {
                addLoadingToHTML();
                loadOneTime = 1;

                getData(countTimeLoad).then((res) => {
                    challenges.push(res);
                    if (
                        $("#easy").is(":checked") == false &&
                        $("#easy-mobile").is(":checked") == false &&
                        $("#normal").is(":checked") == false &&
                        $("#normal-mobile").is(":checked") == false &&
                        $("#hard").is(":checked") == false &&
                        $("#hard-mobile").is(":checked") == false &&
                        $("#extreme").is(":checked") == false &&
                        $("#extreme-mobile").is(":checked") == false
                    ) {
                        renderExercisesAll(res);
                    } else {
                        if ($("#easy").is(":checked") || $("#easy-mobile").is(":checked")) {
                            renderExercisesEasy(res);
                        }
                        if ($("#normal").is(":checked") || $("#normal-mobile").is(":checked")) {
                            renderExercisesNormal(res);
                        }
                        if ($("#hard").is(":checked") || $("#hard-mobile").is(":checked")) {
                            renderExercisesHard(res);
                        }
                        if ($("#extreme").is(":checked") || $("#extreme-mobile").is(":checked")) {
                            renderExercisesExtreme(res);
                        }
                    }

                    removeLoadingToHTML();
                    countTimeLoad++;
                    loadOneTime = 0;
                });
            }
        }
    });
}

const addLoadingToHTML = () => {
    $(".preloader-wrapper").addClass("active");
};

const removeLoadingToHTML = () => {
    $(".preloader-wrapper").removeClass("active");
};
