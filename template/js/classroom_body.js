$(document).ready(function () {
    $(".tabs").tabs();
    convertHTML();
    $(".owl-carousel").owlCarousel({
        loop: false,
        margin: 10,
        items: 1,
        nav: true,
        navText: [
            '<button class="btn btn-small waves-effect waves-light yellow darken-1 m-3 black-text"><i class="material-icons">chevron_left</i></button>',
            '<button class="btn btn-small waves-effect waves-light yellow darken-1 m-3 black-text"><i class="material-icons">chevron_right</i></button>',
        ],
        dots: false,
    });

    $(".owl-nav").addClass("center");
});

const convertHTML = () => {
    let listVideo = $(".video-container");
    for (let i = 0; i < listVideo.length; i++) {
        let str = listVideo[i].textContent;
        listVideo[i].textContent = "";
        html = $.parseHTML(str);
        listVideo[i].innerHTML = str;
    }
};
