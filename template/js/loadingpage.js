$(window).on("load", function () {
    $(".loader-wrapper").fadeOut("slow");
    $("body").removeClass("overflow-hidden");
    $(".parallax").parallax();
    $(".modal").modal();
    $(".dropdown-trigger").dropdown();
});

const getCookie = (cname) => {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(";");
    for (let i = 0; i < ca.length; ++i) {
        let c = ca[i];
        while (c.charAt(0) == " ") {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};

const success = () => {
    let toastHTML = `<i class="material-icons">
                  mood
      </i><br><span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="blue-text text-darken-2">Thành Công!</span></span>`;
    M.toast({ html: toastHTML });
};
