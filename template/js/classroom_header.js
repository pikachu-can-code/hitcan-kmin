$(document).ready(function () {
    $(".collapsible").collapsible();
    $(".toggle-slide").on("click", function () {
        if ($(window).width() >= 992) {
            if ($("#slide-out").hasClass("side-open")) {
                $("#slide-out").removeClass("side-open");
                $("#slide-out").addClass("side-close");
            } else {
                $("#slide-out").addClass("side-open");
                $("#slide-out").removeClass("side-close");
            }
            $("#scale-menu-close")
                .toggleClass("d-block")
                .toggleClass("d-none")
                .toggleClass("scale-out")
                .toggleClass("scale-in");
            $("#scale-menu-open")
                .toggleClass("d-block")
                .toggleClass("d-none")
                .toggleClass("scale-out")
                .toggleClass("scale-in");
            $("#main").toggleClass("not-full").toggleClass("full");
        }
    });

    $(window).bind("resize", function () {
        if ($(window).width() < 992) {
            $("#slide-out").removeClass("side-open");
            $("#slide-out").addClass("side-close");
            $("#main").removeClass("not-full").addClass("full");
            $(".sidenav").sidenav();
        } else {
            $("#slide-out").removeClass("side-close");
            $("#slide-out").addClass("side-open");
            $("#main").removeClass("full").addClass("not-full");
            $(".sidenav").sidenav("destroy");
        }
    });
    checkWidth();
});

function checkWidth() {
    if ($(window).width() < 992) {
        $("#slide-out").removeClass("side-open");
        $("#slide-out").addClass("side-close");
        $("#main").removeClass("not-full").addClass("full");
        $(".sidenav").sidenav();
    } else {
        $("#slide-out").removeClass("side-close");
        $("#slide-out").addClass("side-open");
        $("#main").removeClass("full").addClass("not-full");
        $(".sidenav").sidenav("destroy");
    }
}
