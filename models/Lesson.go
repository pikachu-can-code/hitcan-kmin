package models

import (
	"github.com/gosimple/slug"
	"github.com/jinzhu/gorm"
)

type Lesson struct {
	gorm.Model
	Name      string           `json:"name" gorm:"type:VARCHAR(100)"`
	IDChapter uint             `json:"id_chapter,omitempty" gorm:"foreignkey:chapters"`
	SearchURL string           `json:"searchURL" gorm:""`
	Stt       int              `json:"stt" gorm:""`
	Videos    []VideosOfLesson `json:"videos" gorm:"foreignkey:IDLesson;association_foreignkey:ID"`
	Docs      []Document       `json:"documents" gorm:"foreignkey:IDLesson;association_foreignkey:ID"`
}

func (this *Lesson) Create() {
	db := OpenDB()
	defer db.Close()

	this.SearchURL = slug.MakeLang(this.Name, "en")
	db.Create(&this)
}

func GetAllLessonByChapterID(id uint) []Lesson {
	db := OpenDB()
	defer db.Close()

	var lessons []Lesson
	db.Model(&lessons).Where("id_chapter = ?", id).Find(&lessons)
	return lessons
}

func (this *Lesson) GetLessonByNameLessonAndIDChapter() {
	db := OpenDB()
	defer db.Close()

	db.Where("name = ? and id_class = ?", this.Name, this.IDChapter).Find(&this)
}
