package models

import (
	"fmt"

	"github.com/jinzhu/gorm"
)

type Problem struct {
	gorm.Model
	NameProblem string      `json:"name_problem,omitempty" gorm:"size:300;not null"`
	Premium     int         `json:"premium" gorm:"not null"`
	Challenges  []Challenge `json:"challenges_of_problem,omitempty" gorm:"many2many:threads;"`

	/** TODO: 1 là easy -> 4  */
	Level                int               `json:"level,omitempty" gorm:"not null"`
	ProblemNeedToBeSolve string            `json:"problem_need_to_be_solve,omitempty" gorm:"type:TEXT"`
	Description          string            `json:"description,omitempty" gorm:"type:TEXT"`
	Constraint           string            `json:"constraint,omitempty" gorm:"type:TEXT"`
	InputFormat          string            `json:"input_format,omitempty" gorm:"type:TEXT"`
	OutPutFormat         string            `json:"output_format,omitempty" gorm:"type:TEXT"`
	InputsOutputsFormat  ListInputsOutputs `json:"inputs_outputs_for_problem,omitempty" gorm:"foreignkey:IDProblem;association_foreignkey:ID"`
	LinkYoutubes         ListYoutubeLinks  `json:"youtube_links,omitempty" gorm:"foreignkey:IDProblem;association_foreignkey:ID"`
}

type ListProblems []Problem

func (this *Problem) GetProblem() error {
	db := OpenDB()
	defer db.Close()

	if err := db.First(&this).Error; err != nil {
		return err
	}

	listInputOutput, err := GetInputOutputOfProblem(*this)
	if err != nil {
		return err
	}
	this.InputsOutputsFormat = listInputOutput

	listYoutube := ListYoutubeLinks{}
	listYoutube, err = GetLinksYoutube(*this)
	if err != nil {
		return err
	}
	this.LinkYoutubes = listYoutube

	return nil
}

func (this *Problem) GetProblemByID() error {
	db := OpenDB()
	defer db.Close()

	db.Where("id = ?", this.ID).First(&this)

	return db.Model(&this).Association("Challenges").Find(&this.Challenges).Error
}

func (this *Problem) GetChallengesOfProblem() error {
	db := OpenDB()
	defer db.Close()

	return db.Model(&this).Association("Challenges").Find(&this.Challenges).Error
}

func (this *ListProblems) CoutingListProblem() int {
	db := OpenDB()
	defer db.Close()

	return int(db.Find(&this).RowsAffected)
}

func GetAllProblem() (ListProblems, error) {
	db := OpenDB()
	defer db.Close()

	list := ListProblems{}
	if err := db.Find(&list).Error; err != nil {
		return nil, err
	}

	for i, val := range list {
		if err := val.GetProblem(); err != nil {
			return nil, err
		}

		list[i] = val
	}

	return list, nil
}

func (this *ListProblems) GetProblemByChallengeID(id int) error {
	db := OpenDB()
	defer db.Close()

	tempChallenge := Challenge{}
	tempChallenge.ID = uint(id)

	return db.Model(&tempChallenge).Association("Problems").Find(&this).Error
}

func GetListProblem(list []Problem) error {
	db := OpenDB()
	defer db.Close()

	for i, val := range list {
		if err := val.GetProblem(); err != nil {
			return err
		}
		list[i] = val
	}
	return nil
}

func (this *Problem) CreateProblem(challenges []Challenge) error {
	db := OpenDB()
	defer db.Close()

	db.Create(&this)
	db.First(&this)

	return db.Model(&this).Association("Challenges").Append(&challenges).Error
}

func (this *Problem) UpdateProblem(challenges []Challenge) error {
	db := OpenDB()
	defer db.Close()

	if err := db.Model(&this).Update(&this).Error; err != nil {
		return err
	}

	if err := db.Model(&this).Association("Challenges").Clear().Error; err != nil {
		return err
	}

	return db.Model(&this).Association("Challenges").Append(&challenges).Error
}

func (this *Problem) DeleteProblemById() error {
	db := OpenDB()
	defer db.Close()

	if err := db.Model(&this).Where("id = ?", this.ID).Find(&this).Error; err != nil {
		return err
	}

	for i, val := range this.Challenges {
		if err := val.GetChallenge(); err != nil {
			return err
		}
		this.Challenges[i] = val
	}

	if err := DeleteInputOutputOfProblem(*this); err != nil {
		return err
	}

	if err := DeleteLinkYoutube(*this); err != nil {
		return err
	}

	if err := db.Model(&this).Association("Challenges").Clear().Error; err != nil {
		return err
	}

	return db.Unscoped().Where("id = ?", this.ID).Delete(&this).Error
}

func (this *Problem) RemoveRelationShipProblem() error {
	db := OpenDB()
	defer db.Close()

	temp := Problem{}
	temp.ID = this.ID
	if err := db.Model(&temp).Where("id = ?", temp.ID).Find(&temp).Error; err != nil {
		return err
	}

	fmt.Println(temp)

	if err := DeleteInputOutputOfProblem(temp); err != nil {
		return err
	}

	if err := DeleteLinkYoutube(temp); err != nil {
		return err
	}
	return nil
}

func (this *Problem) GetNextProblem(premiumUser int) uint {
	db := OpenDB()
	defer db.Close()

	temp := Problem{}
	var num int64
	if premiumUser == 0 {
		list := ListProblems{}
		length := db.Model(&list).Find(&list).RowsAffected
		db.Model(&list).Offset(this.ID).Limit(length).Find(&list)
		flag := false
		for _, val := range list {
			if val.Premium == 0 {
				temp.ID = val.ID
				flag = true
				num = 1
				break
			}
		}
		if flag == false {
			return 0
		}
	} else {
		num = db.Model(&this).Offset(this.ID).Limit(1).Find(&temp).RowsAffected
	}
	if num == 1 {
		return temp.ID
	}
	return 0
}
