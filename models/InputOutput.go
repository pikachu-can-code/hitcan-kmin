package models

import (
	"github.com/jinzhu/gorm"
)

type InputOutput struct {
	gorm.Model
	SampleInput  string `json:"sample_input,omitempty" gorm:"type:TEXT"`
	SampleOutput string `json:"sample_output,omitempty" gorm:"tpye:TEXT"`
	IDProblem    uint   `json:"id_problem,omitempty" gorm:"foreignkey:problem"`
}

type ListInputsOutputs []InputOutput

func GetInputOutputOfProblem(problem Problem) (ListInputsOutputs, error) {
	db := OpenDB()
	defer db.Close()

	list := ListInputsOutputs{}
	if err := db.Where("id_problem = ?", problem.ID).Find(&list).Error; err != nil {
		return nil, err
	}
	return list, nil
}

func DeleteInputOutputOfProblem(problem Problem) error {
	db := OpenDB()
	defer db.Close()

	return db.Unscoped().Where("id_problem = ?", problem.ID).Delete(InputOutput{}).Error
}

func (this *InputOutput) CreateExemple() error {
	db := OpenDB()
	defer db.Close()

	return db.Create(&this).Error
}
