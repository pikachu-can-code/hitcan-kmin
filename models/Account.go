package models

import (
	"errors"

	"github.com/jinzhu/gorm"
)

type Account struct {
	gorm.Model
	Email      string   `json:"email,omitempty" gorm:"size:250;unique"`
	Password   string   `json:"password,omitempty" gorm:"type:TEXT"`
	Name       string   `json:"name,omitempty" gorm:"type:TEXT"`
	Phone      string   `json:"phone,omitempty" gorm:"size:12"`
	FacebookID string   `json:"facebook_id,omitempty" gorm:"size:500"`
	GoogleID   string   `json:"google_id,omitempty" gorm:"size:500"`
	Groups     []Group  `json:"groups_of_account,omitempty" gorm:"many2many:account_in_group;"`
	Courses    []Course `json:"courses_of_account,omitempty" gorm:"many2many:account_in_course;"`

	/** 0 là tài khoản không premium, 1 là tài khoản premium */
	Premium        int    `json:"premium" gorm:"default:'0'"`
	LocationAvatar string `json:"location_avatar,omitempty" gorm:"type:TEXT"`
	URLAvatar      string `json:"url_avatar,omitempty" gorm:"type:TEXT"`
}

type ListAccounts []Account

func (this *Account) CreateAccount() error {
	db := OpenDB()
	defer db.Close()

	if !db.NewRecord(this) {
		return errors.New("ID is not blank")
	}

	if db.Create(&this).Error != nil {
		return errors.New("create account error")
	}

	return nil
}

func (this *Account) SelectAccount() error {
	db := OpenDB()
	defer db.Close()

	if err := db.Where("email = ?", this.Email).First(&this).Error; err != nil {
		return err
	}

	if err := db.Model(&this).Association("Groups").Find(&this.Groups).Error; err != nil {
		return err
	}

	if err := db.Model(&this).Association("Courses").Find(&this.Courses).Error; err != nil {
		return err
	}

	return nil
}

func (this *Account) GetAccount() error {
	db := OpenDB()
	defer db.Close()

	return db.Find(&this).Error
}

func (this *Account) Update() error {
	db := OpenDB()
	defer db.Close()

	return db.Model(&Account{}).Update(&this).Error
}

func (this *Account) UpdateWithEmail() error {
	db := OpenDB()
	defer db.Close()

	return db.Model(&Account{}).Where("email = ?", this.Email).Update(&this).Error
}

func (this *Account) Save() error {
	db := OpenDB()
	defer db.Close()

	return db.Model(&Account{}).Save(&this).Error
}

func (this *Account) UpdatePremium() error {
	db := OpenDB()
	defer db.Close()

	temp := Account{}
	temp.ID = this.ID

	if err := db.Where("id = ?", temp.ID).Find(&temp).Error; err != nil {
		return err
	}

	temp.Premium = this.Premium

	return db.Model(&Account{}).Save(&temp).Error
}

func (this *Account) CheckEmailExist() bool {
	db := OpenDB()
	defer db.Close()

	return db.Where("email = ?", this.Email).Find(this).RowsAffected != 0
}

func (this *Account) Delete() (string, error) {
	db := OpenDB()
	defer db.Close()

	if err := db.Model(&this).Association("Groups").Clear().Error; err != nil {
		return "", err
	}

	if err := db.Unscoped().Where("id = ?", this.ID).Delete(&this).Error; err != nil {
		return "", err
	}

	return this.LocationAvatar, nil
}

func (this *Account) CheckInfoLogin() bool {
	db := OpenDB()
	defer db.Close()

	return db.Where(&this).Find(&this).RowsAffected != 0
}

func (this *Account) CheckIDGoogleExist() bool {
	db := OpenDB()
	defer db.Close()

	return db.Where("google_id = ? and email = ?", this.GoogleID, this.Email).Find(&this).RowsAffected != 0
}

func (this *Account) CheckIDFacebookExist() bool {
	db := OpenDB()
	defer db.Close()

	return db.Where("facebook_id = ? and email = ?", this.FacebookID, this.Email).Find(&this).RowsAffected != 0
}

func (this *Account) UpdateAccountFacebook() error {
	db := OpenDB()
	defer db.Close()

	return db.Model(&Account{}).Where("facebook_id = ?", this.FacebookID).Update(&this).Find(&this).Error
}

func (this *Account) SelectAccountFacebook() error {
	db := OpenDB()
	defer db.Close()

	return db.Where("facebook_id = ?", this.FacebookID).Find(&this).Error
}

func (this *ListAccounts) CountingAccounts() int {
	db := OpenDB()
	defer db.Close()

	return int(db.Where("premium < ?", 2).Find(&this).RowsAffected)
}

func (this *ListAccounts) GetIDAccountLimit(begin, limit int) error {
	db := OpenDB()
	defer db.Close()

	temp := Account{}
	return db.Model(&temp).Where("premium < ?", 2).Order("created_at DESC").Offset(begin).Limit(limit).Find(&this).Error
}

func (this *ListAccounts) GetAllAccount() error {
	db := OpenDB()
	defer db.Close()

	return db.Where("premium < ?", 2).Find(&this).Error
}

func (this *Account) GetGroupByAccount() error {
	db := OpenDB()
	defer db.Close()

	return db.Model(&this).Association("Groups").Find(&this.Groups).Error
}

func (this *Account) UpdateAccountInCourse(courseID uint) error {
	db := OpenDB()
	defer db.Close()

	type accInCourse struct {
		AccountID uint `gorm:"account_id"`
		CourseID  uint `gorm:"course_id"`
	}
	data := accInCourse{
		AccountID: this.ID,
		CourseID:  courseID,
	}

	var count int = 0
	db.Table("account_in_course").Where("account_id = ? and course_id = ?", data.AccountID, data.CourseID).Count(&count)
	if count != 0 {
		return nil
	}

	return db.Table("account_in_course").Create(&data).Error
}
