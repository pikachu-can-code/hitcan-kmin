package handlers

import (
	"hitcan-kmin/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetTopic(c *gin.Context) {

	topicID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Topic["no_data_found"],
			},
		})
		return
	}

	topicUint := uint(topicID)
	topic := models.Topic{}
	topic.ID = topicUint

	total := models.CountChallengesOfTopic(topic)

	limit := 7

	loadTimeNow, begin := LoadTimeAPI(c, limit)
	if loadTimeNow == -1 && begin == -1 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Topic["error"],
			},
		})
		return
	}
	maxTimeLoad := total / limit

	if err = topic.GetTopicWithLimitChallenge(begin, limit); err == nil {
		c.JSON(http.StatusOK, gin.H{
			"code":          http.StatusOK,
			"data":          topic,
			"max_time_load": maxTimeLoad,
		})
		return
	}

	c.JSON(http.StatusBadRequest, gin.H{
		"code": http.StatusBadRequest,
		"error": gin.H{
			"message": Topic["no_data_found"],
		},
	})
}

func GetAllTopic(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	topics := models.ListTopics{}
	if err := topics.GetAllTopic(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Topic["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": topics,
	})
	return
}
