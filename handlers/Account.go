package handlers

import (
	"encoding/base64"
	"fmt"
	"hitcan-kmin/models"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

func CreateAccount(c *gin.Context) {
	info := models.Account{}
	if err := c.ShouldBind(&info); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if info.CheckEmailExist() {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["email_exist"],
			},
		})
		return
	}

	info.Password = HashPassWithSalt(info.Password)

	err := info.CreateAccount()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
	})

}

func SendEmail(c *gin.Context) {

	// header := c.GetHeader("Authorization")
	// if header == "" {
	// 	c.JSON(http.StatusBadRequest, gin.H{
	// 		"code": http.StatusBadRequest,
	// 		"error": gin.H{
	// 			"message": Account["not_authorization"],
	// 		},
	// 	})
	// 	return
	// }

	// token, err := b64.URLEncoding.DecodeString(header)
	// if

	info := models.Account{}
	if err := c.ShouldBind(&info); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	codeValidate := RandomString()
	err := SendEmailConfirm(info.Email, codeValidate)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["can_not_send_email"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code":          http.StatusOK,
		"confirm_email": codeValidate,
		"data":          info,
	})
}

func CheckEmailExist(c *gin.Context) {
	info := models.Account{}
	if err := c.ShouldBind(&info); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if info.CheckEmailExist() {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["email_exist"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
	})
}

func DeleteAccount(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		fmt.Println(id, email)
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	info := models.Account{}
	idAccount, err := strconv.Atoi(c.Query("id_user"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}
	info.ID = uint(idAccount)

	locationAvatar, err := info.Delete()
	if err != nil {
		panic(err)
	}

	err = DeleteImage(locationAvatar)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
	})
}

func CheckStatusLogin(c *gin.Context) {
	infoLogin := models.Account{}
	if err := c.ShouldBind(&infoLogin); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	infoLogin.Password = HashPassWithSalt(infoLogin.Password)

	if infoLogin.CheckInfoLogin() {
		tokenString, err := GenToken(infoLogin)
		if err != nil {
			panic(err)
		}

		c.JSON(http.StatusOK, gin.H{
			"code":  http.StatusOK,
			"token": tokenString,
			"data":  infoLogin,
		})
		return
	}

	c.JSON(http.StatusBadRequest, gin.H{
		"code": http.StatusBadRequest,
		"error": gin.H{
			"message": Account["not_match_login"],
		},
	})
}

func Logout(c *gin.Context) {
	DeleteCookie(c, "Authorization", "/", true)
	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
	})
}

func UpdateForgotPassword(c *gin.Context) {
	info := models.Account{}
	if err := c.ShouldBind(&info); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	info.Password = HashPassWithSalt(info.Password)

	if err := info.UpdateWithEmail(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["err_update_password"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
	})
}

type dataUpdatePassword struct {
	OldPassword string `json:"old_password"`
	NewPassword string `json:"new_password"`
}

func UpdatePassword(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	data := dataUpdatePassword{}
	if err := c.ShouldBind(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	data.OldPassword = HashPassWithSalt(data.OldPassword)
	data.NewPassword = HashPassWithSalt(data.NewPassword)

	account.Password = data.OldPassword
	if account.CheckInfoLogin() == false {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	account.Password = data.NewPassword

	if err := account.Update(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["err_update_password"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
	})
}

func UpdateProfile(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	info := models.Account{}
	if err := c.ShouldBind(&info); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	account.Email = info.Email
	account.Name = info.Name
	account.Phone = info.Phone

	if err := account.Update(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["err_update"],
			},
		})
		return
	}

	tokenString, err := GenToken(account)
	if err != nil {
		panic(err)
	}

	c.JSON(http.StatusOK, gin.H{
		"code":  http.StatusOK,
		"token": tokenString,
	})
}

type updateAvatar struct {
	Avatar string `json:"avatar"`
}

func UpdateAvatar(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {

		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	avatar := updateAvatar{}
	err := c.ShouldBind(&avatar)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}
	imageArray1 := strings.Split(avatar.Avatar, ";")
	imageArray2 := strings.Split(imageArray1[1], ",")
	file, err := base64.StdEncoding.DecodeString(imageArray2[1])
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	err = DeleteImage(account.LocationAvatar)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	timeNow := time.Now()
	timeImage := timeNow.Format("2006-01-02T15:04:05")
	nameLink, err := SaveImage(file, "avatar", "avatar_user_"+strconv.Itoa(int(account.ID))+"_"+timeImage+".jpeg")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}
	account.URLAvatar = "http://hit.kmin.edu.vn/" + nameLink
	account.LocationAvatar = nameLink

	if err := account.Update(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code":      http.StatusOK,
		"url_image": account.URLAvatar,
	})
}

func DeleteteAvatar(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {

		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if err := DeleteImage(account.LocationAvatar); err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	account.LocationAvatar = ""
	account.URLAvatar = ""

	if err := account.Save(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code":      http.StatusOK,
		"url_image": "/img/no-user.jpg",
	})
}

func GetNumberOfAccounts(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	var accounts = models.ListAccounts{}
	number := accounts.CountingAccounts()
	c.JSON(http.StatusOK, gin.H{
		"code":               http.StatusOK,
		"number_of_accounts": number,
	})
}

func GetAccountLimit(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	var accounts = models.ListAccounts{}
	allAccount := accounts.CountingAccounts()
	limit := 7
	_, begin := PaginationHandler(c, limit)
	numOfAccount := allAccount / limit
	numOfAccount++

	err := accounts.GetIDAccountLimit(begin, limit)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	for i, val := range accounts {
		if err := val.GetGroupByAccount(); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"code": http.StatusBadRequest,
				"error": gin.H{
					"message": Hitcan["error"],
				},
			})
			return
		}
		accounts[i] = val
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": gin.H{
			"accounts":      accounts,
			"max_time_load": numOfAccount,
		},
	})
}

func GetAllAccount(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	accounts := models.ListAccounts{}
	if err := accounts.GetAllAccount(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": gin.H{
			"list_accounts": accounts,
		},
	})
}

func UpdatePremium(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	info := models.Account{}
	if err := c.ShouldBind(&info); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if err := info.UpdatePremium(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
	})
}
