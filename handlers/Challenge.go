package handlers

import (
	"hitcan-kmin/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetChallenge(c *gin.Context) {
	challengeID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Challenge["error"],
			},
		})
		return
	}
	challengeUint := uint(challengeID)
	challenge := models.Challenge{}
	challenge.ID = challengeUint

	total := models.CountProblemOfChallenge(challenge)
	limit := 8

	loadTimeNow, begin := LoadTimeAPI(c, limit)

	if loadTimeNow == -1 && begin == -1 {
		return
	}
	maxTimeLoad := total / limit
	maxTimeLoad++

	if err := challenge.GetProblemOfChallengeLimit(begin, limit); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Challenge["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code":          http.StatusOK,
		"data":          challenge,
		"max_time_load": maxTimeLoad,
	})
}

func GetAllChallenge(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	list := models.ListChallenges{}
	if err := list.GetAllChallenge(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Challenge["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": gin.H{
			"challenges": list,
		},
	})
}

func GetChallengeByTopicID(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	idTopic, err := strconv.Atoi(c.Query("id_topic"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Challenge["error"],
			},
		})
		return
	}

	list := models.ListChallenges{}

	if err := list.GetChallengeByTopicID(idTopic); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Challenge["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": gin.H{
			"challenges": list,
		},
	})
}

func UpdateChallenge(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	challenge := models.Challenge{}
	if err := c.ShouldBind(&challenge); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Challenge["error"],
			},
		})
		return
	}

	if err := challenge.UpdateChallenge(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Challenge["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
	})
}

func GetChallengeByID(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	idChallenge, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}
	challenge := models.Challenge{}
	challenge.ID = uint(idChallenge)
	if err := challenge.GetChallengeByID(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": gin.H{
			"challenge": challenge,
		},
	})
}

func GetChallengeOfGroup(c *gin.Context) {
	groupID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"error":   err.Error(),
				"message": Hitcan["error"],
			},
		})
		return
	}

	group := models.Group{}
	group.ID = uint(groupID)
	if err := group.GetGroupDetail(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"error":   err.Error(),
				"message": "không tìm thấy kết quả phù hợp",
			},
		})
		return
	}

	total := models.CountChallengesOfGroup(group)
	limit := 7
	loadTimeNow, begin := LoadTimeAPI(c, limit)
	if loadTimeNow == -1 && begin == -1 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"error":   err.Error(),
				"message": Hitcan["error"],
			},
		})
		return
	}
	maxTimeLoad := total / limit

	if err := group.GetChallenge(begin, limit); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"error":   err.Error(),
				"message": Hitcan["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code":          http.StatusOK,
		"data":          group,
		"max_time_load": maxTimeLoad,
	})
	return
}
