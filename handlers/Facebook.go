package handlers

import (
	"fmt"
	"hitcan-kmin/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// func generateStateFacebookCookie() string {
// 	b := make([]byte, 32)
// 	rand.Read(b)
// 	state := base64.URLEncoding.EncodeToString(b)
// 	return state
// }

// func GetConfigFacebook() *oauth2.Config {
// 	//Get Facebook access token
// 	return &oauth2.Config{
// 		ClientID:     FACEBOOK_CLIENT_ID,
// 		ClientSecret: FACEBOOK_CLIENT_SECRET,
// 		RedirectURL:  FACEBOOK_CALLBACK,
// 		Scopes:       []string{"public_profile"},
// 		Endpoint:     facebook.Endpoint,
// 	}
// }

// func OauthFacebookLogin(c *gin.Context) {
// 	oauthState := generateStateFacebookCookie()
// 	u := GetConfigFacebook().AuthCodeURL(oauthState)

// 	c.Redirect(http.StatusTemporaryRedirect, u)
// }

// func GetUserDataFromFacebook(code string) ([]byte, error) {
// 	token, err := GetConfigFacebook().Exchange(context.Background(), code)
// 	if err != nil {
// 		return nil, err
// 	}

// 	response, err := http.Get(OAUTH_FACEBOOK_URL_API + token.AccessToken)
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer response.Body.Close()

// 	contents, err := ioutil.ReadAll(response.Body)
// 	if err != nil {
// 		return nil, err
// 	}
// 	return contents, nil
// }

// func FacebookCallBack(c *gin.Context) {
// 	data, err := GetUserDataFromFacebook(c.Query("code"))
// 	if err != nil {
// 		fmt.Println(err)
// 		c.JSON(http.StatusBadRequest, gin.H{
// 			"code": http.StatusBadRequest,
// 			"error": gin.H{
// 				"message": Facebook["error"],
// 			},
// 		})
// 		return
// 	}

// 	var info gin.H
// 	if err := json.Unmarshal(data, &info); err != nil {
// 		fmt.Println(err)
// 		c.JSON(http.StatusBadRequest, gin.H{
// 			"code": http.StatusBadRequest,
// 			"error": gin.H{
// 				"message": Facebook["error"],
// 			},
// 		})
// 		return
// 	}

// 	fmt.Println(info)
// 	fmt.Println("alo")
// 	name := info["name"].(string)
// 	email := info["email"].(string)
// 	avatar := info["picture"].(string)
// 	id := info["id"].(string)
// 	FacebookLogin(c, id, email, avatar, name)
// }

func FacebookLogin(c *gin.Context) {
	// login facebook nằm bên trang kmin do cần https, khi call back lấy data xong thì chuyển hướng sang đây

	name := c.Query("name")
	id := c.Query("id")
	email := c.Query("email")

	img := "https://graph.facebook.com/" + id + "/picture?width=300&height=300"

	account := models.Account{
		Name:       name,
		FacebookID: id,
		URLAvatar:  img,
		Email:      email,
	}

	if !account.CheckIDFacebookExist() {
		if err := account.CreateAccount(); err != nil {
			fmt.Println(err)
			c.Redirect(http.StatusTemporaryRedirect, "http://hit.kmin.edu.vn/page_not_found")
			return
		}
	} else {
		fmt.Println(account)
		if account.LocationAvatar != "" {
			account.LocationAvatar = ""
			account.URLAvatar = ""
		}

		if err := account.UpdateAccountFacebook(); err != nil {
			fmt.Println(err)
			c.Redirect(http.StatusTemporaryRedirect, "http://hit.kmin.edu.vn/page_not_found")
			return
		}
	}

	fmt.Println(account)
	tokenString, err := GenToken(account)
	if err != nil {
		fmt.Println(err)
		c.Redirect(http.StatusTemporaryRedirect, "http://hit.kmin.edu.vn/page_not_found")
		return
	}

	expiration := time.Now().Add(365 * 24 * time.Hour)

	cookie := http.Cookie{
		Name:    "Authorization",
		Value:   tokenString,
		Expires: expiration,
		Path:    "/",
	}

	http.SetCookie(c.Writer, &cookie)

	if account.Premium == 2 {
		c.Redirect(http.StatusSeeOther, "/admin_index")
		return
	}
	c.Redirect(http.StatusSeeOther, "/")
	return
}
