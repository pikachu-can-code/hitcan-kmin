package handlers

import (
	"crypto/sha512"
	"encoding/hex"
	"hitcan-kmin/models"
	"math/rand"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gopkg.in/gomail.v2"
)

var jwtKey = []byte("lamsaobietduockeylieulieu")

const (
	GOOGLE              = 1
	GOOGLE_LOGIN_FORM   = "/auth/google/login"
	GOOGLE_ACCESS_TOKEN = "https://www.linkedin.com/oauth/v2/accessToken"
	GOOGLE_CALLBACK     = "http://hit.kmin.edu.vn/google_login/call_back"

	GOOGLE_CLIENT_ID     = "311722681839-5e0bsjmkvu6uu95ocbf6vb1r87d5b1rk.apps.googleusercontent.com"
	GOOGLE_CLIENT_SECRET = "w80uq6Lo-Z5_rqFVDj7g3_WT"
	GOOGLE_AUTH_URI      = "https://accounts.google.com/o/oauth2/auth"
	GOOGLE_TOKEN_URI     = "https://oauth2.googleapis.com/token"
	GOOGLE_STATE         = "MnAau8sjaKHsako10lI10xpaxjf"
	OAUTH_GOOGLE_URL_API = "https://www.googleapis.com/oauth2/v2/userinfo?access_token="
)

// const (
// 	FACEBOOK              = 2
// 	FACEBOOK_LOGIN_FORM   = "/auth/facebook/login"
// 	FACEBOOK_ACCESS_TOKEN = "https://www.linkedin.com/oauth/v2/accessToken"
// 	FACEBOOK_CALLBACK     = "https://hitcan.kmin.edu.vn/facebook_login/call_back"

// 	FACEBOOK_CLIENT_ID     = "130769474800440"
// 	FACEBOOK_CLIENT_SECRET = "83e315abe6cbeffa4af7e11a7632ea4b"
// 	FACEBOOK_TOKEN_URI     = "MnAaP09xu27Jhako10lI10xpaxjf"
// 	OAUTH_FACEBOOK_URL_API = "https://graph.facebook.com/me?access_token="
// )

type Claims struct {
	Email string `json:"email"`
	ID    uint   `json:"id"`
	jwt.StandardClaims
}

func ValidateEmail(email string) bool {
	Re := regexp.MustCompile(`^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$`)
	return Re.MatchString(email)
}

func HashPassWithSalt(passSource string) string {
	firstHash := []byte(passSource)
	firstSha := sha512.Sum512(firstHash)
	md5HashInString := hex.EncodeToString(firstSha[:])

	secondHash := []byte(md5HashInString)
	secondSha := sha512.Sum512(secondHash)
	resultPassword := hex.EncodeToString(secondSha[:])
	return resultPassword
}

func randomInt(min, max int) int {
	return min + rand.Intn(max-min)
}

func RandomString() string {
	bytes := make([]byte, 7)

	for i := 0; i < 7; i++ {
		rand.Seed(time.Now().UnixNano())
		bytes[i] = byte(randomInt(65, 90))
	}
	return string(bytes)
}

func SendEmailConfirm(Email string, Code string) error {
	mail := gomail.NewMessage()
	mail.SetHeader("From", "kminacademy@gmail.com")
	mail.SetHeader("To", Email)
	mail.SetHeader("Subject", "Xác thực mã code Kmin Hit Challenge")
	mail.SetBody("text/plain", "Mã xác thực của bạn là "+Code)
	dialer := gomail.NewPlainDialer("smtp.gmail.com", 587, "kminacademy@gmail.com", "mydr3am2019")
	return dialer.DialAndSend(mail)
}

func ParseTokenString(TokenString string) (interface{}, interface{}, error) {

	claims := jwt.MapClaims{}
	_, err := jwt.ParseWithClaims(TokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(jwtKey), nil
	})
	id := claims["id"]
	email := claims["email"]
	return email, id, err
}

func GenToken(account models.Account) (string, error) {
	expirationTime := time.Now().Add(300000 * time.Hour)
	claims := &Claims{
		Email: account.Email,
		ID:    account.ID,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(jwtKey)
}

func DeleteImage(directory string) error {
	if directory == "" {
		return nil
	}

	err := os.Remove("/var/www/hitcan-kmin/" + directory)
	return err
}

func SaveImage(file []byte, Local string, NameFile string) (string, error) {
	nameLink := Local + "/" + NameFile

	f, err := os.Create("/var/www/hitcan-kmin/" + nameLink)
	if err != nil {
		return "", err
	}
	defer f.Close()

	if _, err := f.Write(file); err != nil {
		return "", err
	}

	if err := f.Sync(); err != nil {
		return "", err
	}

	return nameLink, nil
}

func LoadTimeAPI(c *gin.Context, limit int) (int, int) {
	loadTime, err := strconv.Atoi(c.Param("loadtime"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Topic["no_data_found"],
			},
		})
		return -1, -1
	}

	if loadTime <= 0 {
		return 1, 0
	}

	begin := (limit * loadTime) - limit
	return loadTime, begin
}

func PaginationHandler(c *gin.Context, limit int) (int, int) {
	var numOfPage int
	numOfPageStr := c.Param("loadtime")

	if numOfPageStr == "" {
		return 1, 0
	} else {
		var err error
		numOfPage, err = strconv.Atoi(numOfPageStr)
		if err != nil {
			panic(err)
		}
	}

	if numOfPage <= 0 {
		return 1, 0
	}

	begin := (limit * numOfPage) - limit
	return numOfPage, begin
}
