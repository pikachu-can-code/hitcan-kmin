package handlers

import (
	"hitcan-kmin/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

func ConsumDataForAdminAddUser() []models.Course {
	courses := models.GetAllCourse()
	return courses
}

func AdminCreateUser(c *gin.Context) {
	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_login"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_login"],
			},
		})
		return
	}

	type reqData struct {
		CourseID uint                `json:"course_id" binding:"required"`
		Users    models.ListAccounts `json:"users"`
	}

	dataReq := reqData{}
	err := c.ShouldBind(&dataReq)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": err.Error(),
			},
		})
		return
	}

	for _, user := range dataReq.Users {
		if !user.CheckEmailExist() {
			user.Password = HashPassWithSalt(user.Password)
			err := user.CreateAccount()
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{
					"code":    http.StatusInternalServerError,
					"message": err.Error(),
				})
				return
			}
		}
		if err := user.UpdateAccountInCourse(dataReq.CourseID); err != nil {
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{
					"code":    http.StatusInternalServerError,
					"message": err.Error(),
				})
				return
			}
		}
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": "success",
	})
}
