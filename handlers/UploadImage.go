package handlers

import (
	"io"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func UploadImage(c *gin.Context) {
	if err := c.Request.ParseForm(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"error":   err.Error(),
				"message": Hitcan["error"],
			},
		})
		return
	}

	file, header, err := c.Request.FormFile("file")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"error":   err.Error(),
				"message": Hitcan["error"],
			},
		})
		return
	}
	defer file.Close()
	f, err := os.OpenFile("/var/www/hitcan-kmin/img/problem_image/"+header.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"error":   err.Error(),
				"message": Hitcan["error"],
			},
		})
		return
	}
	defer f.Close()
	io.Copy(f, file)
	url := "http://hit.kmin.edu.vn/img/problem_image/" + header.Filename
	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"link": url,
	})
}
